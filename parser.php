<?php
/*
 * @author Alvaro P. Guzman Lopez
 *
 * Listen the Telegram CLI client for requests
 */
$salir = FALSE;
$fr=fopen("php://stdin","r");   // open our file pointer to read from stdin
while(($input = fgets(STDIN)) && $salir===FALSE){
    //Do something with the telegram:
    if (preg_match('#\s+(\d{4})\s+\[(\d\d:\d\d)\]\s+([A-Za-z0-9_]+)\s+...\s+([a-zA-Z0-9]+)#', $input, $match))
    {
        $seqnum = $match[1];
        $dateti = $match[2];
        $source = $match[3];
        $comand = $match[4];
        // Log $seqnum $dateti $source $comand
        echo "__Commando -> $comand";
        if ($comand=='salir')
            $salir=TRUE;
    } else if (preg_match("#salir#", $input))
            $salir=TRUE;
    else if (preg_match("#pwd#", $input))
            echo exec('uname -a');
    else
        print_r($input);
    //echo 'XXXXXXXX->'.$input."\n";
}

?>
